<?php
/**
 * Created by Vir Dignus.
 * Description: add user css and java script in your modx resource
 * @params:
 *  $css-path to your css
 *  $js-path to js
 */
$styles = explode('|', $css);
$scripts = explode('|', $js);
if (!empty($styles)) {
    foreach ($styles as $style) {
        $modx->regClientCSS($style);
    }
}
if(!empty($scripts)){
    foreach ($scripts as $script) {
        $modx->regClientScript($script);
    }
}