<?php
/**
 * @author Vir Dignus
 * @copyright 2013
 * plugin for Modx Revolution for generate introtext and intro image from first image in text or from TV
 * @var  modResource $resource;
 */

$ctx = $resource->context_key;
$templIDs = explode(',', $modx->getOption("templIDs", $scriptProperties, 'all')); //included templates, 'all' mean all templates.
$parentIDs = explode(',', $modx->getOption('parentIDS', $scriptProperties, '0')); //included parentns ids, set parents, for whom will be generate
if ($templIDs[0] == "all") {
    $in_template = true;
} else {
    $in_template = in_array($resource->template, $templIDs);
}
$in_parent = false;
//list of all parent ids
$p_list = $modx->getParentIds($id, 10, array('context' => $ctx));
foreach ($p_list as $p) {
    if (in_array($p, $parentIDs)) {
        $in_parent = true;
        break;
    }
}
$status = '';
if ($in_parent && $in_template) {
    /**
     * option block
     */
    $allowTags = $modx->getOption('allowTags', $scriptProperties, '<b>'); //allowed tags,they not, be stripped
    $img_path = $modx->getOption('img_path', $scriptProperties, MODX_ASSETS_URL .
        "images/");
    $limit = $modx->getOption('limit', $scriptProperties, 600); // substring char limit
    $tv = $modx->getOption('tv_id', $scriptProperties, 1); // id of TV for image
    $def_img = $modx->getOption('def_img', $scriptProperties, 'assets/default.gif'); //default image name
    $def_img2 = $modx->getOption('def_img2', $scriptProperties, 'assets/default.jpg'); //default image name for slider
    $imgHeight = $modx->getOption('imgHeight', $scriptProperties, 120); //heigth of cropped image
    $imgWidht = $modx->getOption('imgWidht', $scriptProperties, 100); //widht of cropped image
    $secondImgHeight = $modx->getOption('secondImgHeight', $scriptProperties, 150); //heigth of cropped image for second thumbs
    $secondIimgWidht = $modx->getOption('secondImgWidht', $scriptProperties, 200); //widht of cropped image for second thumbs
    //$custv = $modx->getObject('modTemplateVar', 1);
    //$img = $custv->getValue($id);
    $img = $resource->GetTVValue($tv); //get TV value
    $img_p = $resource->getProperty('introimage', 'introtext', ''); //get value from proprties field
    $s_img = $resource->getProperty('sliderimage', 'introtext', '');; //using for set second thumb for image, for example biger thumb for slider
    $def_img = $def_img; //set default image
    $text = $resource->content;
    $base_path = '';
    $introtext = $resource->introtext;
    $status = "";
    if ($ctx == "web") {
        $base_path = $modx->getOption('base_path');
    } else {
        $modx->switchContext($ctx);
        $base_path = dirname(MODX_BASE_PATH) . '/' . $modx->getOption('http_host') . '/';
    }
    $matches = array();

    if ($img != $img_p) {
        $status = 'changed';
    }
    if ((empty($img)) and (preg_match_all('|<img.*?src=[\'"](.*?)[\'"].*?>|i', $text,
            $matches)) != 0
    ) {
        $status = 'empty';

    }

    if ($status == "") {
        $status = "ok";
    }
    switch ($status) {
        case 'empty':
            $img = $matches[1][0];
            if (empty($img)) {
                $status = 'ok';
                $img = $def_img;
                $s_img = $def_img2;
            }
            break;
        case 'changed':
            if (empty($img)) {
                $status = 'ok';
                $img = $def_img;
                $s_img = $def_img2;
            } else {
                $img = $img;
            }
            break;
    }


    if ($status != "ok" and is_readable($base_path . $img)) {
        //check folder for thumbs
        if (!(file_exists($base_path . dirname($img) . '/thumbs/'))) {
            mkdir($base_path . dirname($img) . '/thumbs/', 0755);
        }

        try {
            $img2 = '';
            $img2 = $img;
            $image = new Imagick($base_path . $img);

            $image->cropThumbnailImage($imgWidht, $imgHeight);

            // $image->setImageFormat('png');
            $thumb = $base_path . dirname($img) . '/thumbs/' . $imgWidht . 'x' . $imgHeight . '_' .
                basename($img);

            $image->writeImage($thumb);
            $img = dirname($img) . '/thumbs/' . $imgWidht . 'x' . $imgHeight . '_' .
                basename($img);
            if ($secondIimgWidht || $secondImgHeight) {
                $image->cropThumbnailImage($secondIimgWidht, $secondImgHeight);
                $s_thumb = $base_path . dirname($img2) . '/thumbs/' . $secondIimgWidht . 'x' . $secondImgHeight .
                    '_' . basename($img2);
                $image->writeImage($s_thumb);
                $s_img = dirname($img2) . '/thumbs/' . $secondIimgWidht . 'x' . $secondImgHeight .
                    '_' . basename($img2);

            }
            // Destroys Imagick object, freeing allocated resources in the process

            $image->destroy();
        } catch (ImagickException $e) {
            $modx->log(1, $e->getMessage());
            $image->destroy();
            $img = $def_img;
            $s_img = $def_img2;
        }

    } /*else {
        $img = $def_img;
        $s_img = $def_img2;
    }*/

    $resource->setTVValue($tv, $img);

    $resource->setProperties(array('introimage' => $img, 'sliderimage' => $s_img), 'introtext');
    if ($introtext == "") {
        $resource->set('introtext', mb_substr(strip_tags($text, $allowTags), 0, $limit,
            'UTF-8'));

    }

//   if( (strlen($resource->alias))>5){$resource->set('alias',$id);}
    $resource->save();




}