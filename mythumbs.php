<?php
/**
 * User: Voloshchuk Yuri aka virdignus
 * Date: 07.08.13
 * Time: 9:38
 * snippet for generate image thumbs
 *You can use it like filter
 *	 [[+tv.intro-image:mythumbs=`widht|height|1|path\to\replace\image`]]
 */
$path = $modx->getOption('base_path');
$default_path = "assets/images/thumbs/"; //path for thumbs
$thumbs_path = $modx->getOption("thumbs_path", $scriptProperties, $default_path);
list($w, $h, $rc, $di) = explode("|", $options);
$w = empty($w) ? 100 : $w;
$h = empty($h) ? 100 : $h;
$rc = empty($rc) ? 1 : $rc;
$image = !empty($input) ? $path . $input : '';
if($image=='')
{
    $image = empty($di)? "http://placehold.it/".$w.'x'.$h   : $di;
}
 

elseif (file_exists($thumbs_path . $w . "x" . $h . "_" . basename($image))) {
    $image = $thumbs_path . $w . "x" . $h . "_" . basename($image);

}
else {
    try {
        $imagick = new Imagick($path . $input);
        $imagick->resizeImage($w, 0, imagick::FILTER_LANCZOS, 1);
        $imagick->cropthumbnailimage($w,$h);
        $th = $path . $thumbs_path . $w . "x" . $h . "_" . basename($image);
        $imagick->writeimage($th);
        $imagick->destroy();


    } catch (ImagickException $e) {
        $modx->log(1, $e->getMessage());
	    $image = empty($di)? "http://placehold.it/".$w.'x'.$h   : $di;


    }
}

return $image;